/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrizbinaria;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author wfgp1
 */
public class SistemaMatrizBinaria {
    boolean m1[][];
    boolean m2[][];
    
    public SistemaMatrizBinaria() {
    }

    public SistemaMatrizBinaria(int fila_m1, int col_m1,int fila_m2, int col_m2) {
        if(fila_m1>Math.pow(2, col_m1)){
            throw new RuntimeException("numero filas de la matriz m1 excede la cantidad maxima");            
        }
        m1 = new boolean[fila_m1][col_m1];
        llenar(m1);
        if(fila_m2>Math.pow(2, col_m2)){
            throw new RuntimeException("numero filas de la matriz m2 excede la cantidad maxima");            
        }
        m2 = new boolean[fila_m2][col_m2];
        llenar(m2);
    }   
    
    public void llenar(boolean [][]x){
        boolean carry = false;
        for (int i = 1; i < x.length; i++) {
            for (int j = i; j < x.length; j++) {
                for (int k = x[j].length-1; k >= 0; k--) {
                    if (carry) {
                        if(!x[j][k]){
                            x[j][k]=true;
                            carry=false;
                            k=0;
                        }else
                            x[j][k]=false;
                    }else{
                        if(x[j][k]){
                            x[j][k]=false;
                            carry=true;
                        }else{
                            x[j][k]=true;
                            k=0;
                        }
                    }
                }                            
            }
        }        
    }
    
    
    public boolean[][] getMultiplicacion(){
        if(m1[0].length!=m2.length){
            throw new RuntimeException("No se pueden multiplicar las matrices");            
        }        
            boolean[][] C = new boolean[m1.length][m2[0].length];
            for (int i = 0; i < C.length; i++) {
                for (int j = 0; j < C[i].length; j++) {
                    for (int k=0; k<m2.length; k++){
                        if (m1[i][k]==true && m2[k][j]==true) {
                            C[i][j] = true;
                            k=m2.length;
                        }
                    } 
                }
            }
        return C;
    }
   
    
   
    public boolean comparar(byte[][]x){
        byte[] aux = new byte[x.length];
        boolean rta = true;
        for (int i = 0; i < x.length; i++) {            
            byte suma=0;
            byte exp = (byte)(x[i].length-1);
            for (int j = 0; j < x[i].length; j++) {
                suma += (x[i][j]==1)? Math.pow(2, exp):0;
                exp--;
            }
            if (i!=0) {                       
                if (rta) {
                    for (int k = 0; k < i; k++) {
                        if(aux[k]==suma){
                            rta=false;
                            k=i;    
                        }
                    }    
                }
            }
            aux[i]=suma;
        }
        return rta;
    }
    
    public boolean[][] getM1() {
        return m1;
    }

    public void setM1(boolean[][] m1) {
        this.m1 = m1;
    }

    public boolean[][] getM2() {
        return m2;
    }

    public void setM2(boolean[][] m2) {
        this.m2 = m2;
    }

    @Override
    public String toString() {
        return "SistemaMatrizBinaria{" + "m1=" + m1 + ", m2=" + m2 + '}';
    }
    
}
