/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrizbinaria;

/**
 *
 * @author wfgp1
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        SistemaMatrizBinaria x = new SistemaMatrizBinaria(6,4,4,2);
        System.out.println("La matriz m1 es:"+"\n");
        for (int i=0; i < x.getM1().length; i++) {
            System.out.print("|");
            for (int j=0; j < x.getM1()[i].length; j++) {
                System.out.print (x.getM1()[i][j]);
                if (j!=x.getM1()[i].length-1) System.out.print("\t");
                }
                System.out.println("|");
        }
        System.out.println("\n");
        
        System.out.println("La matriz m2 es:"+"\n");
        for (int i=0; i < x.getM2().length; i++) {
            System.out.print("|");
            for (int j=0; j < x.getM2()[i].length; j++) {
                System.out.print (x.getM2()[i][j]);
                if (j!=x.getM2()[i].length-1) System.out.print("\t");
                }
                System.out.println("|");
        }
        System.out.println("\n");

        
        for (int i=0; i < x.getMultiplicacion().length; i++) {
            System.out.print("|");
            for (int j=0; j < x.getMultiplicacion()[i].length; j++) {
                System.out.print (x.getMultiplicacion()[i][j]);
                if (j!=x.getMultiplicacion()[i].length-1) System.out.print("\t");
                }
                System.out.println("|");
        }
        
    }
    
}
